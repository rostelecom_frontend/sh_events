# Виджет камеры для дашборда

Подключение npm модуля из локальной папки в package.json проекта

```
"dependencies": {
  "vc-camera-events": "file:./path_to_folder"
}
```

Использование в реакт компоненте

```
import CameraEventsdWidget from 'vc-camera-events';

<CameraEventsdWidget {...options} />
```

Доступные опции

```
defaultProps = {
    uid: '',        // uid камеры, обязательный параметр
    proxyUrl: '',   // адрес прокси апи,
    cookieUrl: '',  // хост для получения куки
    session: PropTypes.shape({
        'X-User-Key': PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        'X-User-Token': PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        'HTTP_HOME_ID': PropTypes.oneOfType([PropTypes.string, PropTypes.number])     
    })

    // обработчик клика на событие в списке (для смены таймстемпа плеера через location)
    handleEventClick: (uid, timestamp) => { console.info('---> click event', uid, timestamp); },
}
```

Пример инициализации

```
var options = {
    uid: '123456-1234-1233-1234-1234567890',            // uid камеры
    proxyUrl: 'http://smart.32team.com/services/vc',    // или просто '/services/vc' если запросы на том же домене
    cookieUrl: 'https://demo.videocomfort.com',         // хост для получения куки, обязательный параметр
    handleEventClick: callbackFunctionName                   // имя функции-обработчика клика (вернет uid камеры и  стартовый таймстемп выбранного события в списке)
}

return (<CameraEventsdWidget {...options} />)
```
